//Structs and Enums - Part 1
//Paul Mutimba 500189343

#include <iostream>
#include <conio.h>
#include <string>

using namespace std;
enum Rank { Two = 2, Three, Four, Five, Six, Seven, Eight, Nine, Ten, Jack, Queen, King, Ace };
enum Suit { Hearts, Spades, Diamonds, Clubs };

struct Card
{
	Rank Rank;
	Suit Suit;

};

void PrintCard(Card Card) {
	if (Card.Rank == 2 && Card.Suit == 0) //HEARTS
	{
		cout << "The Two of Hearts.\n";
	}
	else if(Card.Rank == 3 && Card.Suit == 0)
	{
		cout << "The Three of Hearts.\n";
	}
	else if (Card.Rank == 4 && Card.Suit == 0)
	{
		cout << "The Four of Hearts.\n";
	}
	else if (Card.Rank == 5 && Card.Suit == 0)
	{
		cout << "The Five of Hearts.\n";
	}
	else if (Card.Rank == 6 && Card.Suit == 0)
	{
		cout << "The Six of Hearts.\n";
	}
	else if (Card.Rank == 7 && Card.Suit == 0)
	{
		cout << "The Seven of Hearts.\n";
	}
	else if (Card.Rank ==8 && Card.Suit == 0)
	{
		cout << "The Eight of Hearts.\n";
	}
	else if (Card.Rank == 9 && Card.Suit == 0)
	{
		cout << "The Nine of Hearts.\n";
	}
	else if (Card.Rank == 10 && Card.Suit == 0)
	{
		cout << "The Jack of Hearts.\n";
	}
	else if (Card.Rank == 11 && Card.Suit == 0)
	{
		cout << "The Queen of Hearts.\n";
	}
	else if (Card.Rank == 12 && Card.Suit == 0)
	{
		cout << "The King of Hearts.\n";
	}
	else if (Card.Rank == 13 && Card.Suit == 0)
	{
		cout << "The Ace of Hearts.\n";
	}

	if (Card.Rank == 2 && Card.Suit == 1) //SPADES
	{
		cout << "The Two of Spades.\n";
	}
	else if (Card.Rank == 3 && Card.Suit == 1)
	{
		cout << "The Three of Spades.\n";
	}
	else if (Card.Rank == 4 && Card.Suit == 1)
	{
		cout << "The Four of Spades.\n";
	}
	else if (Card.Rank == 5 && Card.Suit == 1)
	{
		cout << "The Five of Spades.\n";
	}
	else if (Card.Rank == 6 && Card.Suit == 1)
	{
		cout << "The Six of Spades.\n";
	}
	else if (Card.Rank == 7 && Card.Suit == 1)
	{
		cout << "The Seven of Spades.\n";
	}
	else if (Card.Rank == 8 && Card.Suit == 1)
	{
		cout << "The Eight of Spades.\n";
	}
	else if (Card.Rank == 9 && Card.Suit == 1)
	{
		cout << "The Nine of Spades.\n";
	}
	else if (Card.Rank == 10 && Card.Suit == 1)
	{
		cout << "The Jack of Spades.\n";
	}
	else if (Card.Rank == 11 && Card.Suit == 1)
	{
		cout << "The Queen of Spades.\n";
	}
	else if (Card.Rank == 12 && Card.Suit == 1)
	{
		cout << "The King of Spades.\n";
	}
	else if (Card.Rank == 13 && Card.Suit == 1)
	{
		cout << "The Ace of Spades.\n";
	}

	if (Card.Rank == 2 && Card.Suit == 2) //DIAMONDS
	{
		cout << "The Two of Diamonds.\n";
	}
	else if (Card.Rank == 3 && Card.Suit == 2)
	{
		cout << "The Three of Diamonds.\n";
	}
	else if (Card.Rank == 4 && Card.Suit == 2)
	{
		cout << "The Four of Diamonds.\n";
	}
	else if (Card.Rank == 5 && Card.Suit == 2)
	{
		cout << "The Five of Diamonds.\n";
	}
	else if (Card.Rank == 6 && Card.Suit == 2)
	{
		cout << "The Six of Diamonds.\n";
	}
	else if (Card.Rank == 7 && Card.Suit == 2)
	{
		cout << "The Seven of Diamonds.\n";
	}
	else if (Card.Rank == 8 && Card.Suit == 2)
	{
		cout << "The Eight of Diamonds.\n";
	}
	else if (Card.Rank == 9 && Card.Suit == 2)
	{
		cout << "The Nine of Diamonds.\n";
	}
	else if (Card.Rank == 10 && Card.Suit == 2)
	{
		cout << "The Jack of Diamonds.\n";
	}
	else if (Card.Rank == 11 && Card.Suit == 2)
	{
		cout << "The Queen of Diamonds.\n";
	}
	else if (Card.Rank == 12 && Card.Suit == 2)
	{
		cout << "The King of Diamonds.\n";
	}
	else if (Card.Rank == 13 && Card.Suit == 2)
	{
		cout << "The Ace of Diamonds.\n";
	}

	if (Card.Rank == 2 && Card.Suit == 3) //CLUBS
	{
		cout << "The Two of Clubs.\n";
	}
	else if (Card.Rank == 3 && Card.Suit == 3)
	{
		cout << "The Three of Clubs.\n";
	}
	else if (Card.Rank == 4 && Card.Suit == 3)
	{
		cout << "The Four of Clubs.\n";
	}
	else if (Card.Rank == 5 && Card.Suit == 3)
	{
		cout << "The Five of Clubs.\n";
	}
	else if (Card.Rank == 6 && Card.Suit == 3)
	{
		cout << "The Six of Clubs.\n";
	}
	else if (Card.Rank == 7 && Card.Suit == 3)
	{
		cout << "The Seven of Clubs.\n";
	}
	else if (Card.Rank == 8 && Card.Suit == 3)
	{
		cout << "The Eight of Clubs.\n";
	}
	else if (Card.Rank == 9 && Card.Suit == 3)
	{
		cout << "The Nine of Clubs.\n";
	}
	else if (Card.Rank == 10 && Card.Suit == 3)
	{
		cout << "The Jack of Clubs.\n";
	}
	else if (Card.Rank == 11 && Card.Suit == 3)
	{
		cout << "The Queen of Clubs.\n";
	}
	else if (Card.Rank == 12 && Card.Suit == 3)
	{
		cout << "The King of Clubs.\n";
	}
	else if (Card.Rank == 13 && Card.Suit == 3)
	{
		cout << "The Ace of Clubs.\n";
	}
}

int main()
{

	for (Suit i = Hearts; i <= Clubs; i = Suit(i + 1)) {
		for (Rank j = Two; j <= Ace; j = Rank(j + 1)) {

			Card card;
			card.Rank = j;
			card.Suit = i;
			PrintCard(card);
		}
	}


	(void)_getch();
	return 0;
}

